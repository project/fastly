<?php


namespace Drupal\fastly;


interface CacheTagsHashInterface {

  /**
   * Default Cache tag hash length.
   */
  const CACHE_TAG_HASH_LENGTH = 4;
}
