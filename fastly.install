<?php

/**
 * @file
 * Install, update, and uninstall functions for the Fastly module.
 */

use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Implements hook_requirements().
 */
function fastly_requirements($phase) {
  $requirements = [];

  if ($phase == 'runtime') {

    // Check API token to be sure it has not been removed from
    // Fastly or expired.
    $api = \Drupal::service('fastly.api');
    $state = \Drupal::service('fastly.state');

    $purge_credentials_are_valid = $api->validatePurgeCredentials();
    $state->setPurgeCredentialsState($purge_credentials_are_valid);

    if (!$purge_credentials_are_valid) {
      $link_to_settings = Url::fromRoute('fastly.settings')->toString();
      $message = t('Missing valid purge credentials for Fastly. Please go <a href=":link_to_settings">here</a> to set.', [':link_to_settings' => $link_to_settings]);

      $requirements['fastly'] = [
        'title' => t('Fastly - Credentials'),
        'severity' => REQUIREMENT_ERROR,
        'description' => $message,
      ];
    }
  }
  return $requirements;
}

/**
 * Purge cache after module update.
 */
function fastly_update_8001(&$sandbox) {
  $link = Link::createFromRoute('/admin/config/services/fastly','fastly.settings');
  return t("Cache tag hashing algorithm has changed with the latest module version so you'll need to purge your entire service cache manually if you currently use cache tags. You can do that by either visiting fastly settings form (@link) and clicking on 'Purge whole service' action or going directly to your fastly dashboard and do it from there.", [
    '@link' =>  $link->toString()
  ]);
}
